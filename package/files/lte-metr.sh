#!/bin/bash
#DOWNLOAD_SOURCE=http://cdimage.ubuntu.com/releases/18.04/release/ubuntu-18.04.1-server-arm64.template
DOWNLOAD_SOURCE=http://185.68.215.129/1GB.bin
IFACE=wwan0

function wait_to_full_second {
    # require coreutils-date package
    # get the number of nanoseconds, round it to hundreds of microseconds
    # (use uppermost 4 digits), mark it decadic (#10) and substract it from 10000
    # The result is number of remaining hundreds of microseconds to wait
    sleep 0.$(printf '%04d' $((10000 - 10#$(/usr/bin/date +%4N))))
}
function download {
    /usr/bin/wget -O /dev/null -o /dev/null $DOWNLOAD_SOURCE &
    PID=$!

    # kill downloading thread on exit
    trap "kill $PID 2> /dev/null" EXIT

    echo "Time,Lat,Long,RSRP,SINR Rx[0],RSRQ,RSSI,IP Thrpt DL,MCC,MNC,LAC,Cell Id,PCI,DL EARFCN,Bandwidth,QPSK Rate,16-QAM Rate,64-QAM Rate,256-QAM Rate,Carrier Aggregation DL"

    OLD_COUNT=$(cat /sys/class/net/$IFACE/statistics/rx_bytes)
    # Check whether the file is still being downloaded
    while kill -0 $PID 2> /dev/null; do
        wait_to_full_second
        COUNT=$(cat /sys/class/net/$IFACE/statistics/rx_bytes)
        let "KBPS=($COUNT-$OLD_COUNT)*8/1000"
        RESLINE=$(date +"%d.%m.%Y %H:%M:%S")","$(/usr/bin/lteinfo.py)",,,,"
        eval echo "$RESLINE"
        OLD_COUNT=$COUNT
    done

    # Disable the trap on a normal exit.
    trap - EXIT
}

my_sn=`atsha204cmd serial-number`

while true; do
    # Check that default route is via LTE interface
    while [ -z "$(ip r | grep default | grep wwan)" ]; do sleep 1; done;
    echo "Starting download" | logger -t "lte-metr"
    /bin/mkdir -p /tmp/lte-metr
    download 2>&1 1>"/tmp/lte-metr/`/usr/bin/date +'%Y-%m-%d_%H-%M-%S'`_$my_sn.csv" | logger -t "lte-metr"
    echo "Download finished, starting netmetr" | logger -t "lte-metr"
    /usr/bin/netmetr-lte --set-gps-console /dev/ttyUSB0 2>&1 1>/dev/null | logger -t "lte-metr"
done

